package cn.edu.ujn.ch13.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.edu.ujn.ch13.model.User;

@Controller
public class HelloController {
    @RequestMapping(value = "/first")
    public String handleRequest(Model model) {
        model.addAttribute("msg", "这是我的第一个基于注解的SpringMVC程序!---");
        return "first";
    }

    @GetMapping(value = "/index")
    public String index() {
        return "login";
    }

    @RequestMapping("/login")
    public String login(User user) {
        System.out.println(user);
        if ("liukun".equals(user.getUsername())) {
            user.setUsername("刘鹍");
        }
        return "ok";
    }
}
