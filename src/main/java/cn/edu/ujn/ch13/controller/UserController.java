package cn.edu.ujn.ch13.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.edu.ujn.ch13.model.Orders;
import cn.edu.ujn.ch13.model.User;
import cn.edu.ujn.ch13.model.UserVO;

@Controller
public class UserController {
	
	/**
	 * 1、绑定默认数据类型
	 * @param HttpServletRequest 
	 * @return String
	 */
	@RequestMapping("/selectUser")
	public String selectUser(HttpServletRequest requeset) {	
		// TODO 完善从HttpServletRequest获取id
		String id = requeset.getParameter("id");
		System.out.println(id);
		requeset.setAttribute("id", id);
		return "success";
	}
	
	/**
	 * 2、绑定简单数据类型
	 * @param Integer  
	 * @return String
	 */
	@RequestMapping("/findUser")
	public String findUser(Integer id) {
		// TODO 完善获取id
		System.out.println(id);
		return "success";
}
	
	/**
	 * 3、绑定POJO数据类型
	 * @param User  
	 * @return String
	 */	
	@RequestMapping("/toRegister")
	public String toRegister() {
		return "register";
	}
	// TODO 完善/registerUser （绑定POJO）
	@RequestMapping("/registerUser")
	public String registerUser(User user) {
		System.out.println(user);
		return "ok";
	}
	
	/**
	 * 4、绑定包装POJO数据类型
	 * @param User  
	 * @return String
	 */	
	@RequestMapping("/orders")
	public String tofindOrdersWithUser( ) {
	    return "orders";
	}
	// TODO 完善/findOrdersWithUser（完善绑定包装POJO）
	@RequestMapping("/findOrdersWithUser")
	public String findOrdersWithUser(Orders orders) {
	   System.out.println(orders);
		return "success";
	}
	
	/**
	 * 5、绑定复杂数据-绑定数组-删除用户
	 * @param Integer[]   
	 * @return String
	 */		
    @RequestMapping("/delete")
    public String toDeleteUser(Model model) {
    	List<User> userList = new ArrayList<User>();
		User u1 = new User();
		u1.setId(1);
		userList.add(u1);
		u1.setUsername("liukun");
		User u2 = new User();
		u2.setId(2);
		u2.setUsername("jingshan");
		userList.add(u2);
		// TODO 完善传递userList 到页面
		model.addAttribute("userList",userList);
        return "deleteUser";
    }
    // TODO 添加/deleteUsers（完善绑定数组）
    @RequestMapping("/deleteUsers")
	public String deleteUsers(Integer ids[]) {
    	for(Integer id: ids)
    		System.out.println(id);
    	
	    return "success";
	}
    
    /**
	 * 6、绑定复杂数据-绑定集合-修改用户
	 * @param UserVO    
	 * @return String
	 */
    @RequestMapping("/edit")
    public String toEditUser() {
        return "editUser";
    }
    // TODO 添加/editUsers（完善绑定集合）
    @RequestMapping("/editUsers")
	public String editUsers(UserVO users) {
    	System.out.println(users);
	    return "success";
	}

}
