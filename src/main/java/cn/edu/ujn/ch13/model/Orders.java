package cn.edu.ujn.ch13.model;

public class Orders {

	private Integer ordersId; 
	private User user;
	
	public Integer getOrdersId() {
		return ordersId;
	}
	public void setOrdersId(Integer ordersId) {
		this.ordersId = ordersId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "Orders [ordersId=" + ordersId + ", user=" + user + "]";
	}
	
	
}
