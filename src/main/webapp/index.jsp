<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<html>
<body>
<h2>Hello World!</h2>
<h2><a href="selectUser?id=555" target="_blank">1、绑定默认数据类型：/selectUser?id=555</a></h2><br>
<h2><a href="findUser?id=888"  target="_blank">2、绑定简单数据类型：/findUser?id=888</a></h2><br>
<h2><a href="toRegister"  target="_blank">3、绑定POJO数据类型：/toRegister</a></h2><br>
<h2><a href="orders"  target="_blank">4、绑定包装POJO数据类型：/orders</a></h2><br>
<h2><a href="delete"  target="_blank">5、绑定复杂数据-绑定数组-删除用户：/delete</a></h2><br>
<h2><a href="edit"  target="_blank">6、绑定复杂数据-绑定集合-修改用户：/edit</a></h2><br>
</body>
</html>
